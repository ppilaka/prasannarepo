package com.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.BasePage;
import com.components.NavigationBarComponent;
import com.components.PopupManageSubTasks;

public class MyTasksPage extends BasePage {

	// This is the constructor of the class
	public MyTasksPage(WebDriver driver){
		//this.driver=driver;
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//h1")
	public WebElement headerThisIsYourToDoList;
	
	@FindBy(xpath="//input[@id='new_task']")
	public WebElement textBoxEnterNewTask;
	
	@FindBy(xpath="//span[contains(@class,'glyphicon-plus')]")
	public WebElement buttonAddNewTask;
	
	@FindBy(xpath="//tbody//tr//td[5]")
	public List<ToBeDoneTableElements> allToBeDoneRows;
	
	public PopupManageSubTasks getPopupManageSubTasks(){
		return (new PopupManageSubTasks(driver, "//div[contains(@class,'modal-content')]"));
	}
	
	public NavigationBarComponent getNavigationLinkName(){
		return (new NavigationBarComponent(driver, "//div[contains(@class,'navbar-default') and @role='navigation']"));
	}
	
	public boolean isMyTasksLinkDisplayedOnNavigationBar(){
		return getNavigationLinkName().linkMyTasks.isDisplayed();
	}

	public String verifyLoggedInUserDisplayMessage(String message){
		String messageToReturn = null;
		if(headerThisIsYourToDoList.getText().toString().equals(message)){
			messageToReturn= "The logged in user name is displayed correct";
		} else {
			messageToReturn="The logged in user name is displayed NOT correct";
		}
		return messageToReturn;
	}
	
	/** This method finds the number of subtasks added for a particular task which is in the form of an indicator for the Manage Subtasks button on each row on the table
	 * 
	 * @return returns the number of subtasks for a given task 
	 * */
	public int getNumberOfSubTasksCreatedForMyTask(String toBeDoneRowText){
		int i=0;
		String buttonText = getManageTasksButton(toBeDoneRowText).linkManageSubTasks.getText().toString();
		i = Integer.parseInt(buttonText.substring(1, 2));
		return i;
	}
	
	
	/** 
	 * This class contains individual elements in the table (ToBeDone) on My Tasks Page. 
	 * */
	public class ToBeDoneTableElements extends BasePage {
		@FindBy(xpath=".//td[1]")
		public WebElement checkBoxDone;

		@FindBy(xpath=".//td[2]")
		public WebElement textTodo;

		@FindBy(xpath=".//td[3]")
		public WebElement checkBoxPublic;

		@FindBy(xpath=".//td[4]")
		public WebElement linkManageSubTasks;

		@FindBy(xpath=".//td[5]")
		public WebElement buttonRemoveTask;

		private String parentXpath = null;
		public ToBeDoneTableElements(WebDriver driver, String xpath){
			super(driver);
			parentXpath = xpath;
			PageFactory.initElements(driver, this);
		}
	}
	
	
	/** This method finds the exact row of the newly added task in the ToBeDone list on My Tasks Page 
	 * 
	 * Usage at Test Case level: String rowWithTaskFound = myTasksPage.getToBeDoneRow(<<task text in one row, you want to search in the table>>);
	 *	 						 System.out.println(rowWithTaskFound);
	 * @return matchFoundText
	 * */
	public String getToBeDoneRow(String toBeDoneRowText){
		String matchFoundText = null;
		String locator = "//tbody//tr[.//td//a[text()='"+toBeDoneRowText+"']]";
		
		List<WebElement> rowMatch = driver.findElements(By.xpath(locator));
		
		if(rowMatch.size()==0){
			matchFoundText = "No matching task found in the table.";
		} else if(rowMatch.size()==1){
			matchFoundText = "The Newly added task "+toBeDoneRowText+"added is present in the table.";
		} else {
			matchFoundText = "More than one task with the same name found in the table.";
		}
		return matchFoundText;
	}
		
	
	/** This method fetches the row with the newly added task in the ToBeDone list and click on the Manage Tasks button, on My Tasks Page
	 * 
	 *  Usage at Test Case level: myTasksPage.clickOnManageTasksButton(<<task text in one row, you want to click>>).linkSubTasks.click();
	 *  
	 *  @return ToBeDoneTableElements - which are the individual elements in the table
	 *  @exception 
	 *  */
	public ToBeDoneTableElements getManageTasksButton(String toBeDoneRowText){
		
		String locator = "//tbody//tr[.//td//a[text()='"+toBeDoneRowText+"']]";
		try{
			List<WebElement> rowMatch = driver.findElements(By.xpath(locator));
			
			if(rowMatch.size()==0){
				System.out.println("No matching task found in the table.");
			} else if(rowMatch.size()==1){
				rowMatch.get(0);
			} else {
				System.out.println("More than one task with the same name found in the table.");
			}
		}catch(Exception e){
			System.out.println("There is an exception trying ot click on Manage Tasks button on My Tasks Page --: " +e.getMessage());
		}
		
		return (new ToBeDoneTableElements(driver, locator));
	}
	
	
	/** This method will check if the first row is the newly added Task or not
	 * 
	 * @return true/false
	 * */
	public boolean isFirstTaskTheNewlyAddedTask(String taskNameJustAdded){
		boolean isFirst = false;
		if(getFirstTask().textTodo.getText().toString().equals(taskNameJustAdded)){
			isFirst= true;
		} else {
			isFirst = false;
		}
		return isFirst;
	}
	
	/** This method brings the first row of the table - ToBeDone 
	 * 
	 * @return ToBeDoneTableElements of the first row
	 * @throws   NoSuchElementException */
	public ToBeDoneTableElements getFirstTask() throws NoSuchElementException{
		if(allToBeDoneRows.size()>0){
			return allToBeDoneRows.get(0);
		} else
			throw (new NoSuchElementException("The row is not found"));
	}
	
	
	public void deleteAllTasksForCleanUp(){
		if(allToBeDoneRows.size()>0){
		for(int i=0; i<allToBeDoneRows.size(); i++){
			allToBeDoneRows.get(i).buttonRemoveTask.click();
		}
		} else {
			System.out.println("There are no rows to delete.");
		}
	}
}

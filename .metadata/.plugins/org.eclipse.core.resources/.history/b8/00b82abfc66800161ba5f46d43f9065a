package com.testsuite;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.common.BaseTestCase;
import com.pages.MyTasksPage;
import com.pages.SignInPage;
import com.pages.WelcomePage;
import com.utils.TODO_VAR;

public class TODO_Tests extends BaseTestCase {

	public WebDriver driver;
	SignInPage signInPage;
	
	@BeforeClass(alwaysRun = true)
	public void setUp(){
		this.driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.navigate().to(TODO_VAR.LOGIN_URL);
		signInPage = PageFactory.initElements(driver, SignInPage.class);
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown(){
		driver.quit();
	}
	
	
	@Test(dataProvider="testcaseData")
	public void TODO_Test1(String username, String password){
	
		signInPage.linkSignIn.click();
		Assert.assertTrue(signInPage.isMyTasksLinkDisplayedOnNavigationBar());
		
		signInPage.enterEmail(username);
		signInPage.enterPassword(password);
		signInPage.clickSignInButton();
		
		WelcomePage welcomePage = new WelcomePage(driver);
		welcomePage.isloginSuccess();
		welcomePage.verifyLoggedInUser();
		Assert.assertTrue(welcomePage.isMyTasksLinkDisplayedOnNavigationBar());
		welcomePage.getNavigationLinkName().linkMyTasks.click();
		
		MyTasksPage myTasksPage = new MyTasksPage(driver);
		Assert.assertEquals(myTasksPage.headerThisIsYourToDoList.getText().toString(), "Hey Prasanna, this is your todo list for today:", "The logged in user name is displayed NOT correct.");
		
	}

	@Test(dataProvider="testcaseData")
	public void TODO_Test2(String username, String password){
		
		signInPage.linkSignIn.click();
		
		signInPage.enterEmail(username);
		signInPage.enterPassword(password);
		signInPage.clickSignInButton();
		
		WelcomePage welcomePage = new WelcomePage(driver);
		welcomePage.getNavigationLinkName().linkMyTasks.click();
		
		MyTasksPage myTasksPage = new MyTasksPage(driver);
		
		myTasksPage.textBoxEnterNewTask.sendKeys("First Task");
		myTasksPage.textBoxEnterNewTask.sendKeys(Keys.ENTER);
		myTasksPage.textBoxEnterNewTask.sendKeys("Second Task");
		myTasksPage.buttonAddNewTask.click();
	
		Assert.assertTrue(myTasksPage.isFirstTaskTheNewlyAddedTask("Second Task"), "The task name added is NOT appended to the top of the ToDo List but appears on the bottom");
		
	}
	

	@Test(dataProvider="testcaseData")
	public void TODO_Test3(String username, String password){
		
		signInPage.linkSignIn.click();
		
		signInPage.enterEmail(username);
		signInPage.enterPassword(password);
		signInPage.clickSignInButton();
		
		WelcomePage welcomePage = new WelcomePage(driver);
		welcomePage.getNavigationLinkName().linkMyTasks.click();
		
		MyTasksPage myTasksPage = new MyTasksPage(driver);
		
		myTasksPage.textBoxEnterNewTask.sendKeys("");
		myTasksPage.buttonAddNewTask.click();
		myTasksPage.textBoxEnterNewTask.sendKeys("a");
		myTasksPage.buttonAddNewTask.click();
		myTasksPage.textBoxEnterNewTask.sendKeys("ab");
		myTasksPage.buttonAddNewTask.click();
		myTasksPage.textBoxEnterNewTask.sendKeys("abc");
		myTasksPage.buttonAddNewTask.click();
		
		String moreThans251Characters = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
				+ "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
				+ "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrst";
		myTasksPage.textBoxEnterNewTask.sendKeys(moreThans251Characters);
		myTasksPage.buttonAddNewTask.click();
		
		String exactly250Characters = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
				+ "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
				+ "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopq";
		myTasksPage.textBoxEnterNewTask.sendKeys(exactly250Characters);
		myTasksPage.buttonAddNewTask.click();
		
		String only249Characters = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
				+ "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
				+ "abcdefghijklmnopqrstuvwxyzabcdefghijklmnop";
		myTasksPage.textBoxEnterNewTask.sendKeys(only249Characters);
		myTasksPage.buttonAddNewTask.click();
		
	}
	
	

	@Test(dataProvider="testcaseData")
	public void TODO_Test4(String username, String password){
		
		signInPage.linkSignIn.click();
		
		signInPage.enterEmail(username);
		signInPage.enterPassword(password);
		signInPage.clickSignInButton();
		
		WelcomePage welcomePage = new WelcomePage(driver);
		welcomePage.getNavigationLinkName().linkMyTasks.click();
		
		MyTasksPage myTasksPage = new MyTasksPage(driver);
		myTasksPage.textBoxEnterNewTask.sendKeys("Main Task");
		myTasksPage.buttonAddNewTask.click();
		
		String rowWithTaskFound = myTasksPage.getToBeDoneRow("Main Task");
		System.out.println(rowWithTaskFound);
		
		Assert.assertTrue(myTasksPage.getManageTasksButton("Main Task").linkManageSubTasks.isDisplayed(), "Manage Subtasks button is not displayed for 'Main Task' task.");
	
		int num = myTasksPage.getNumberOfSubTasksCreatedForMyTask("Main Task");
		System.out.println("The number of subTasks for my task is -"+ num +" displayed on Manage Subtasks button on ToDo List page");
		
	}
	

	@Test(dataProvider="testcaseData")
	public void TODO_Test5(String username, String password){
		
		signInPage.linkSignIn.click();
		
		signInPage.enterEmail(username);
		signInPage.enterPassword(password);
		signInPage.clickSignInButton();
		
		WelcomePage welcomePage = new WelcomePage(driver);
		welcomePage.getNavigationLinkName().linkMyTasks.click();
		
		MyTasksPage myTasksPage = new MyTasksPage(driver);
		myTasksPage.textBoxEnterNewTask.sendKeys("First Main Task");
		myTasksPage.buttonAddNewTask.click();
		myTasksPage.getManageTasksButton("First First Task").linkManageSubTasks.click();
		
		myTasksPage.getPopupManageSubTasks().headerEditingTasks.getText().toString().contains("Editing Tasks");
		Assert.assertTrue(myTasksPage.getPopupManageSubTasks().textParentTask.getAttribute("value").toString().equals("First Main TasK"), "The Task Name is not displayed on the Manage Subtasks modal popup");
		
	}
	
	@Test(dataProvider="testcaseData")
	public void TODO_Test6(String username, String password){
		
		signInPage.linkSignIn.click();
		
		signInPage.enterEmail(username);
		signInPage.enterPassword(password);
		signInPage.clickSignInButton();
		
		WelcomePage welcomePage = new WelcomePage(driver);
		welcomePage.getNavigationLinkName().linkMyTasks.click();
		
		MyTasksPage myTasksPage = new MyTasksPage(driver);
		myTasksPage.textBoxEnterNewTask.sendKeys("First Main Task");
		myTasksPage.buttonAddNewTask.click();
		myTasksPage.getManageTasksButton("First First Task").linkManageSubTasks.click();
		
		myTasksPage.getPopupManageSubTasks().textBoxNewSubTask.sendKeys("First SubTask");
		myTasksPage.getPopupManageSubTasks().textBoxDueDate.sendKeys("08/21/2016");
		myTasksPage.getPopupManageSubTasks().buttonAddSubTask.click();
		
		String subTaskRowWithFound = myTasksPage.getPopupManageSubTasks().getSubTasksRow("First SubTask");
		System.out.println(subTaskRowWithFound);
		
	}
}

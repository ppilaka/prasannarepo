package com.components;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.common.BasePage;

public class PopupManageSubTasks extends BasePage {
	
	private String parentXpath = null;
	
	//This is the constructor of the class
	public PopupManageSubTasks(WebDriver driver, String xpath){
		super(driver);
		parentXpath=xpath;
		PageFactory.initElements(driver, this);
		
	}
	
	
	@FindBy(xpath=".//h3[contains(@class,'modal-title')]")
	public WebElement headerEditingTasks;
	
	@FindBy(xpath=".//textarea[@id='edit_task']")
	public WebElement textParentTask;
	
	@FindBy(xpath=".//input[@id='new_sub_task']")
	public WebElement textBoxNewSubTask;
	
	@FindBy(xpath=".//input[@id='dueDate']")
	public WebElement textBoxDueDate;
	
	@FindBy(xpath=".//button[@id='add-subtask']")
	public WebElement buttonAddSubTask;
	
	@FindBy(xpath=".//button[contains(@class,'btn-primary') and text()='Close']")
	public WebElement buttonClose;
	
	
	
	
	/** 
	 * This class contains individual elements in the table (ToBeDone) on My Tasks Page. 
	 * */
	public class SubTaskTableElements extends BasePage {
		@FindBy(xpath=".//td[1]")
		public WebElement checkBoxDone;

		@FindBy(xpath=".//td[2]")
		public WebElement textSubTask;

		@FindBy(xpath=".//td[3]")
		public WebElement buttonRemoveSubTask;

		private String parentXpath = null;
		public SubTaskTableElements(WebDriver driver, String xpath){
			super(driver);
			parentXpath = xpath;
			PageFactory.initElements(driver, this);
		}
	}
	
	/** This method finds the exact row of the newly added SubTask in the "Editing task" Model Popup 
	 * 
	 * Usage at Test Case level: String subTaskRowWithFound = myTasksPage.getPopupManageSubTasks().getSubTasksRow(<<SubTask text to search for>>);
	 *							 System.out.println(subTaskRowWithFound);
	 *
	 * @return subTaskMatchFoundText
	 * */
	public String getSubTasksRow(String subTaskRowText){
		String subTaskMatchFoundText = null;
		String locator = "//tbody//tr[.//td//a[starts-with(text(),'"+subTaskRowText+"')]]";
		
		List<WebElement> rowMatch = driver.findElements(By.xpath(locator));
		System.out.println("Size of the rowMatch is--- "+ rowMatch.size());
		
		if(rowMatch.size()==0){
			subTaskMatchFoundText = "No matching task found in the table.";
		} else if(rowMatch.size()==1){
			subTaskMatchFoundText = "The Newly added task "+subTaskRowText+"added is present in the table.";
		} else {
			subTaskMatchFoundText = "More than one task with the same name found in the table.";
		}
		return subTaskMatchFoundText;
	}
		

}

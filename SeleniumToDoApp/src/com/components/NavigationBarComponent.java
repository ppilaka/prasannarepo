package com.components;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NavigationBarComponent {

    public WebDriver driver;
    public String parentXpath;
	
	@FindBy(xpath= ".//a[normalize-space(text())='ToDo App']")
	public WebElement headerLinkToDoApp;
	
	@FindBy(xpath=".//a[normalize-space(text())='Home']")
	public WebElement linkHome;
	
	@FindBy(xpath=".//a[normalize-space(text())='My Tasks']")
	public WebElement linkMyTasks;
	
	@FindBy(xpath=".//a[normalize-space(text())='Sign In']")
	public WebElement linkSignIn;
	
	@FindBy(xpath=".//a[normalize-space(text())='Register']")
	public WebElement linkRegister;
	
	@FindBy(xpath=".//a[starts-with(text(),'Welcome,')]")
	public WebElement textWelcome;

	@FindBy(xpath=".//a[normalize-space(text())='Sign out']")
	public WebElement linkSignOut;
	
	public boolean myTasksIsDisplayed(){
		return linkMyTasks.isDisplayed();
	}
	//This is the constructor of the class
	public NavigationBarComponent(WebDriver driver, String xpath){
		this.driver=driver;
		this.parentXpath=xpath;
		PageFactory.initElements(driver, this);
	}
	
}

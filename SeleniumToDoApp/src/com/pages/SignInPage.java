package com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.common.BasePage;
import com.components.NavigationBarComponent;
import com.utils.Log;

public class SignInPage extends BasePage{

	@FindBy(xpath="//li[.//a[text()='Sign In']]")
	public WebElement linkSignIn;
	
	@FindBy(xpath="//input[@id='user_email']")
	public WebElement textBoxEmail;
	
	@FindBy(xpath="//input[@id='user_password']")
	public WebElement textBoxPassword;
	
	@FindBy(xpath="//input[contains(@class,'btn-primary') and @value='Sign in']")
	public WebElement buttonSignIn;
	
	@FindBy(xpath="//div[contains(@class,'navbar-default') and @role='navigation']")
	public NavigationBarComponent navigationBarComponent;
	
	// This is the constructor of the class
	public SignInPage(WebDriver driver){
		super(driver);
	}
	
	public void enterEmail(String userName){
		textBoxEmail.sendKeys(userName);
	}
	
	public void enterPassword(String password){
		textBoxPassword.sendKeys(password);
	}
	
	public void clickSignInButton(){
		buttonSignIn.click();
	}
	
	public NavigationBarComponent getNavigationLinkName(){
		return (new NavigationBarComponent(driver, "//div[contains(@class,'navbar-default') and @role='navigation']"));
	}
	
	public boolean isMyTasksLinkDisplayedOnNavigationBar(){
		Log.info("Inside SignInPage - isMyTasksLinkDisplayedOnNavigationBar() method");
		return getNavigationLinkName().linkMyTasks.isDisplayed();
	}
}

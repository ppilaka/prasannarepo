package com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.common.BasePage;
import com.components.NavigationBarComponent;
import com.utils.TODO_VAR;

public class WelcomePage extends BasePage{

	
	@FindBy(xpath="//div[contains(@class,'alert-info') and normalize-space(text()='Signed in successfully.')]")
	public WebElement textSignedInSuccessfully;
	
	@FindBy(xpath="//ul[contains(@class,'navbar-right')]//a[starts-with(text(),'Welcome,')]")
	public WebElement textWelcome;

	@FindBy(xpath="//ul[contains(@class,'navbar-nav')]//a[normalize-space(text())='Sign out']")
	public WebElement linkSignOut;
	
	public NavigationBarComponent getNavigationLinkName(){
		return (new NavigationBarComponent(driver, "//div[contains(@class,'navbar-default') and @role='navigation']"));
	}
	
	public boolean isloginSuccess(){
		return textSignedInSuccessfully.getText().toString().equals(TODO_VAR.SUCCESSFUL_SIGNIN);
	}
	
	public boolean verifyLoggedInUser(){
		return textWelcome.getText().toString().equals(TODO_VAR.WELCOME_USERNAME);
	}
	
	public boolean isMyTasksLinkDisplayedOnNavigationBar(){
		return getNavigationLinkName().linkMyTasks.isDisplayed();
	}
	
	// This is the constructor of the class
	public WelcomePage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
    }
}

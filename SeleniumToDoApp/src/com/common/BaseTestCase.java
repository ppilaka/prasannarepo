package com.common;

import org.testng.Assert;
import org.testng.annotations.DataProvider;

import com.utils.Log;


public class BaseTestCase {

	public static StringBuffer errorVerify;
	@DataProvider(name="testcaseData")
	public static Object[][] testcaseData(){
		return new Object[][]{
				{"prasanna.pilaka@yahoo.com", "Selenium123"}
		};
	}
	
	public static void verifyTrue(boolean condition, String falseStatement, String trueStatement){
		try{
			if(condition==true){
				Assert.assertTrue(condition);
				Log.info(trueStatement);
			} else {
				Log.error(falseStatement);
			}
		}catch(AssertionError e){
			errorVerify.append(e.getMessage() + falseStatement + "\n");
			Log.error(errorVerify.toString());
		}
	}
	
	
}
